/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author dvillanueva
 */
public class EscuhaLimpiar implements ActionListener {
    AccionBoton quienLlama;
    public EscuhaLimpiar(AccionBoton listadoUsuario) {
        this.quienLlama = listadoUsuario;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.quienLlama.limpiarAccion();
    }
}
