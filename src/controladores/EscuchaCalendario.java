/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.util.Date;
/**
 *
 * @author Anthony
 */
public class EscuchaCalendario implements PropertyChangeListener {
    AccionCalendario quienllama;
    public EscuchaCalendario(AccionCalendario quienllama) {
        this.quienllama = quienllama;
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if ("date".equals(e.getPropertyName())) {
            quienllama.cambiarContenido(DateFormat.getDateInstance().format((Date) e.getNewValue()));
        }
    }
}
