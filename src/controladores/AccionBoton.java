package controladores;

/**
 *
 * @author Hector Alvarez
 */
public interface AccionBoton {
    
    public void buscarAccion();
    public void limpiarAccion();
    public void nuevoAccion();
    public void editarAccion();
    public void eliminarAccion();
    public void imprimirAccion();
    public void exportarAccion();
}