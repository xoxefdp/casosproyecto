/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import vistas.Login;

/**
 *
 * @author dvillanueva
 */
public class EscuchaAceptar implements ActionListener{
    AccionBotonFormulario quienLlama;
    public EscuchaAceptar(AccionBotonFormulario quienLlama){
        this.quienLlama = quienLlama;
    }
    @Override
    public void actionPerformed(ActionEvent e) {       
        quienLlama.aceptarAccion();
    }
}
