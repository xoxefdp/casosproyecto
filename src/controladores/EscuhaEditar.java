/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author dvillanueva
 */
public class EscuhaEditar implements ActionListener {
    AccionBoton quienLlama;
    public EscuhaEditar(AccionBoton quienLlama) {
        this.quienLlama = quienLlama;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.quienLlama.editarAccion();
    }
}
