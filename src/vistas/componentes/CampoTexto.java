package vistas.componentes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Panel con borde que contiene un campo de texto y que puede incluir o no un
 * titulo.
 * @author Hector Alvarez
 */
public class CampoTexto extends JPanel{
    private JTextField campo;
    private JPanel panelIn = new JPanel();
    private JButton boton;
    /**
     * Crea un panel con título en el borde, con un campo de texto dentro de
     * ancho visible igual al parametro ancho.
     * 
     * @param nombre titulo a colocar en el borde del panel
     * @param ancho ancho visible del campo de texto incluido en el panel
     */
    public CampoTexto(String nombre, int ancho){
        setLayout(new FlowLayout());
        this.setBackground(Colores.transparente);
        panelIn.setBackground(Colores.transparente);
        campo       = new JTextField(ancho);
        panelIn.setLayout(new FlowLayout());
        panelIn.setBorder(BorderFactory.createTitledBorder(nombre));
        panelIn.add(campo);
        add(panelIn);
    }
    
    /**
     * Crea un panel sin título en el borde, con un campo de texto dentro de
     * ancho visible con valor por defecto de 20.
     */
    public CampoTexto(){
        this(null, 20);
    }
    
    /**
     * Devuelve un valor String con el contenido del campo de texto.
     * @return contenido del campo de texto.
     */
    public String obtenerContenido(){
        return campo.getText();
    }
    
    /**
     * Actualiza el contenido del campo de texto.
     * @param contenido con el que se actualizara el campo de texto.
     */
    public void cambiarContenido(String contenido){
        campo.setText(contenido);
    }
    
    /**
     * Devuelve un valor int con la longuitud del contenido del campo de texto.
     * @return longuitud del campo de texto.
     */
    public int longuitudDelContenido(){
        return obtenerContenido().length();
    }
    
    /**
     * Devuelve un valor int con la longuitud del contenido del campo de texto.
     * @param onOff true el campo es editable, false es no editable.
     */
    public void hacerEditable(boolean onOff){
        campo.setEditable(onOff);
    }
    public void agregarCampoColor(Color color){
        campo.setBackground(color);
    }
    public void agregarBoton (JButton boton) {
        this.boton = boton;
        boton.setSize(campo.getWidth(), campo.getHeight());
        panelIn.add(boton);
    }
    public void agregarBotonEscucha(ActionListener accion) {
        boton.addActionListener(accion);
    }
}