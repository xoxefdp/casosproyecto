package vistas.componentes;

import com.toedter.calendar.JDateChooser;
import controladores.AccionCalendario;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Panel con borde que contiene un campo de texto y que puede incluir o no un
 * titulo.
 * @author Hector Alvarez
 */
public class CampoFecha extends JPanel implements AccionCalendario {
    private JDateChooser campo;
    private String valor;
    private Date fecha;
    private String formato = "dd/MM/yyyy";
    private JPanel panelIn = new JPanel();
    private ImageIcon icono;
       
    /**
     * Crea un panel con título en el borde, con un campo de texto dentro de
     * ancho visible igual al parametro ancho.
     * 
     * @param nombre titulo a colocar en el borde del panel
     */
    public CampoFecha(String nombre){
        setLayout(new FlowLayout());
        this.setBackground(Colores.transparente);
        panelIn.setBackground(Colores.transparente);
        campo       = new JDateChooser();
        campo.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("date".equals(evt.getPropertyName())) {
                    if(evt.getNewValue() != null) {
                        fecha = (Date) evt.getNewValue();
                        valor = DateFormat.getDateInstance().format((Date) evt.getNewValue());
                    }
                }
        }
        });
        panelIn.setLayout(new FlowLayout());
        panelIn.setBorder(BorderFactory.createTitledBorder(nombre));
        panelIn.add(campo);
        add(panelIn);
    }
    
    /**
     * Crea un panel sin título en el borde, con un campo de texto dentro de
     * ancho visible con valor por defecto de 20.
     */
    public CampoFecha(){
        this(null);
    }
    
    /**
     * Devuelve un valor String con el contenido del campo de texto.
     * @return contenido del campo de texto.
     */
    public String obtenerContenido(){
        if(fecha  != null){
            return DateFormat.getDateInstance().format(fecha);
        } else {
            return "";
        }
        
    }
    
    /**
     * Actualiza el contenido del campo de texto.
     * @param contenido con el que se actualizara el campo de texto.
     */
    public void cambiarContenido(String contenido) {
        this.valor = contenido;
        if(!valor.isEmpty()) {
            try {
                this.fecha = new SimpleDateFormat(this.formato).parse(valor);
            } catch (ParseException ex) {
                Logger.getLogger(CampoFecha.class.getName()).log(Level.SEVERE, null, ex);
            }
            campo.setDate(fecha);
        } else {
            campo.setDate(null);
            fecha = null;
        }
    }
    
    public void cambiarIcono(String iconUrl) {
        this.icono = new ImageIcon(iconUrl);
        campo.setIcon(icono);
    }
    /**
     * Devuelve un valor int con la longuitud del contenido del campo de texto.
     * @return longuitud del campo de texto.
     */
    public int longuitudDelContenido(){
        return obtenerContenido().length();
    }
    
    /**
     * Devuelve un valor int con la longuitud del contenido del campo de texto.
     * @param onOff true el campo es editable, false es no editable.
     */
    public void hacerEditable(boolean onOff){
        campo.getDateEditor().setEnabled(onOff);
    }
}