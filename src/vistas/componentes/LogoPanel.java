/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 *
 * @author Anthony
 */
public class LogoPanel extends JPanel {

    private BufferedImage image;

    public LogoPanel() {
        this.setBackground(Colores.transparente);
        URL url = getClass().getResource("/vistas/imagenes/logo.png");
        ImageIcon pic = new ImageIcon(url.getPath());
        Image images = pic.getImage();
        Image newimg = images.getScaledInstance(375, 112, java.awt.Image.SCALE_SMOOTH);
        this.add(new JLabel(new ImageIcon(newimg)));
    }
}