/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.componentes.tablero;

import static SQL.MensajesDeError.errorSQL;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import modelos.Casos;
import modelos.Empresas;
import modelos.Usuarios;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import vistas.componentes.Colores;

/**
 *
 * @author Anthony
 */
public class CasosEmpresasBarChart extends JPanel {
    private String titulo = "";
    private JFreeChart chart;
    public CasosEmpresasBarChart() {
        this.setLayout(new BorderLayout());
        this.setBackground(Colores.blanco);
        final CategoryDataset dataset = createDataset();
        chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        //chartPanel.setPreferredSize(new Dimension(500, 270));
        this.add(chartPanel);        
    }

    public void actualizar() {
        this.removeAll();
        final CategoryDataset dataset = createDataset();
        chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        this.add(chartPanel);        
    }
    private CategoryDataset createDataset() {
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        Empresas empresas = new Empresas();
        ResultSet datos = empresas.sqlrRegistrosBarTablero();
        
        try{
           while(datos.next()){
               dataset.addValue(Double.parseDouble(datos.getString("nuevo")), "Nuevo", datos.getString("empresa"));
               dataset.addValue(Double.parseDouble(datos.getString("asignado")), "Asignado", datos.getString("empresa"));
               dataset.addValue(Double.parseDouble(datos.getString("cerrado")), "Cerrado", datos.getString("empresa"));
               titulo = datos.getString("fecha_filtro");
            }
        }
        catch(SQLException e){
            String mensaje = errorSQL(e.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(2000);
        }        
        return dataset;
        
    }
    
    /**
     * Creates a sample chart.
     * 
     * @param dataset  the dataset.
     * 
     * @return The chart.
     */
    private JFreeChart createChart(final CategoryDataset dataset) {
        
        // create the chart...
        final JFreeChart chart = ChartFactory.createBarChart(
            "Casos por Empresa: " + titulo,       // chart title
            "Empresas",               // domain axis label
            "Valores",                  // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, // orientation
            true,                    // include legend
            true,                     // tooltips?
            false                     // URLs?
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...

        // set the background color for the chart...
        chart.setBackgroundPaint(Color.white);
        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setUpperMargin(0.15);
        //rangeAxis.setLowerBound(-1.0);
        
        // disable bar outlines...
        final CategoryItemRenderer renderer = plot.getRenderer();
        renderer.setSeriesItemLabelsVisible(0, Boolean.TRUE);
        renderer.setSeriesPaint(0, Colores.azul2);
        renderer.setSeriesPaint(1, Colores.amarillo2);
        renderer.setSeriesPaint(2, Colores.verde2);        
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,TextAnchor.TOP_CENTER  ));
        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

        // OPTIONAL CUSTOMISATION COMPLETED.
        
        return chart;
        
    }    
}
