/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.componentes.Botones;
import vistas.componentes.CampoTexto;
import Componentes.ComboTabla;
import com.itextpdf.text.DocumentException;
import controladores.AccionBoton;
import controladores.EscuhaBuscar;
import controladores.EscuhaEditar;
import controladores.EscuhaExportar;
import controladores.EscuhaImprimir;
import controladores.EscuhaLimpiar;
import controladores.EscuhaNuevo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import modelos.Empresas;
import modelos.Usuarios;
import vistas.componentes.Colores;
import vistas.reportes.ReporteExcelUsuarioLista;
import vistas.reportes.ReportePdfUsuarioLista;

/**
 *
 * @author dvillanueva
 */
public class ListaUsuarios extends JPanel implements AccionBoton {
    JPanel panelFiltro, panelFiltroTextos, panelFiltroBotonera, panelTabla;
    CampoTexto usuarioTexto, usuarioCedulaTexto, usuarioNombreTexto;
    ComboTabla usuarioEmpresa, departUsuario;
    String[] arrayBotones = {"Buscar","Limpiar","Nuevo","Editar","Imprimir Pdf","Exportar Excel"};
    Color[] arrayBotonesColores = {Colores.azul,Colores.amarillo2,Colores.azul2,Colores.verde2,Colores.rojo2,Colores.verde};
    ImageIcon[] arrayBotonesIconos = {new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-28-search.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-551-erase.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/social.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-151-edit.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/interface-1.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/interface.png"))};
    Botones botonesFiltro;
    private final TablaUsuarios tablaUsuarios;
    private final Usuarios usuarios;
    private final Empresas empresa;
    private Contenido contenido;

    public ListaUsuarios(Contenido contenido){
        this();
        this.contenido = contenido;
    }
    public ListaUsuarios() {
        this.setBackground(Colores.blanco);
        this.setLayout(new BorderLayout());
        
        panelFiltro = new JPanel();
        panelFiltro.setPreferredSize(new Dimension(400,150));
        panelFiltro.setBackground(Colores.blanco);
        panelFiltro.setLayout(new GridLayout(2,1));
        panelFiltroTextos = new JPanel();
        panelFiltroTextos.setBackground(Colores.blanco);
        panelFiltroTextos.setSize(50,100);
        panelFiltroTextos.setLayout(new FlowLayout());
        usuarioTexto = new CampoTexto("Usuario",12);
        panelFiltroTextos.add(usuarioTexto);
        usuarioCedulaTexto = new CampoTexto("Cedula",12);
        panelFiltroTextos.add(usuarioCedulaTexto);
        usuarioNombreTexto = new CampoTexto("Nombre y Apellido",12);
        panelFiltroTextos.add(usuarioNombreTexto);
        usuarioEmpresa = new ComboTabla("Empresa");
        empresa = new Empresas();
        usuarioEmpresa.cargarCombo("0",empresa.sqlrRegistros());
        panelFiltroTextos.add(usuarioEmpresa);
        departUsuario = new ComboTabla("Departamentos");
        departUsuario.cargarCombo("0",Usuarios.getUsuarioDepartamento());
        panelFiltroTextos.add(departUsuario);
        panelFiltroBotonera = new JPanel();
        panelFiltroBotonera.setBackground(Colores.blanco);
        botonesFiltro = new Botones(arrayBotones,arrayBotonesColores,arrayBotonesIconos);
        botonesFiltro.setBackground(Colores.blanco);
        botonesFiltro.asignarOyente(0, new EscuhaBuscar(this));
        botonesFiltro.asignarOyente(1, new EscuhaLimpiar(this));
        botonesFiltro.asignarOyente(2, new EscuhaNuevo(this));
        botonesFiltro.asignarOyente(3, new EscuhaEditar(this));
        botonesFiltro.asignarOyente(4, new EscuhaImprimir(this));
        botonesFiltro.asignarOyente(5, new EscuhaExportar(this));
        panelFiltroBotonera.add(botonesFiltro);
        panelFiltro.add(panelFiltroTextos);
        panelFiltro.add(panelFiltroBotonera);
        
        panelTabla = new JPanel();
        panelTabla.setLayout(new BorderLayout());
        panelTabla.setBackground(Colores.azulOscuro2);
        
        tablaUsuarios = new TablaUsuarios();
        usuarios      = new Usuarios();
        tablaUsuarios.cargarTabla(usuarios.sqlrRegistros());
        panelTabla.add(tablaUsuarios);
        
        this.add(panelFiltro, BorderLayout.NORTH);
        this.add(panelTabla, BorderLayout.CENTER);
    }
    
    @Override
    public void buscarAccion() {
        String idComboEmpre = ("0".equals(usuarioEmpresa.obtenerId())?"":usuarioEmpresa.obtenerId());
        String idComboDepart = ("0".equals(departUsuario.obtenerId())?"":departUsuario.obtenerId());
        tablaUsuarios.cargarTabla(usuarios.sqlrRegistros(usuarioTexto.obtenerContenido(),
                                                         usuarioCedulaTexto.obtenerContenido(),
                                                         usuarioNombreTexto.obtenerContenido(),
                                                         idComboEmpre, idComboDepart));
    }

    @Override
    public void limpiarAccion() {
        usuarioTexto.cambiarContenido("");
        usuarioCedulaTexto.cambiarContenido("");
        usuarioNombreTexto.cambiarContenido("");
        usuarioEmpresa.posicionarCombo(0);
        departUsuario.posicionarCombo(0);
        buscarAccion();
    }

    @Override
    public void nuevoAccion() {
        new EditarUsuarios(0,tablaUsuarios,contenido);
    }

    @Override
    public void editarAccion() {
        new EditarUsuarios(tablaUsuarios.obtenerId(),tablaUsuarios,contenido);
    }

    @Override
    public void eliminarAccion() {
        
    }

    @Override
    public void imprimirAccion() {
        String idComboEmpre = ("0".equals(usuarioEmpresa.obtenerId())?"":usuarioEmpresa.obtenerId());
        String idComboDepart = ("0".equals(departUsuario.obtenerId())?"":departUsuario.obtenerId());
        try {
            new ReportePdfUsuarioLista(usuarioTexto.obtenerContenido(),
                    usuarioCedulaTexto.obtenerContenido(),
                    usuarioNombreTexto.obtenerContenido(),
                    idComboEmpre, idComboDepart);
        } catch (DocumentException ex) {
        }
    }

    @Override
    public void exportarAccion() {
        String idComboEmpre = ("0".equals(usuarioEmpresa.obtenerId())?"":usuarioEmpresa.obtenerId());
        String idComboDepart = ("0".equals(departUsuario.obtenerId())?"":departUsuario.obtenerId());
        try {
            new ReporteExcelUsuarioLista(usuarioTexto.obtenerContenido(),
                    usuarioCedulaTexto.obtenerContenido(),
                    usuarioNombreTexto.obtenerContenido(),
                    idComboEmpre, idComboDepart);
        } catch (IOException ex) {
        } catch (SQLException ex) {
        }
    }
}
