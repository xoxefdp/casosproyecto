/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import Componentes.ModeloDeTabla;
import static SQL.MensajesDeError.errorSQL;
import controladores.AccionTablaItem;
import controladores.EscuchaTablaItem;
import java.awt.BorderLayout;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import modelos.Usuarios;
import vistas.componentes.Colores;
import vistas.librerias.ReizableJTable;

/**
 *
 * @author dvillanueva
 */
public class TablaUsuarios extends JPanel implements AccionTablaItem {
    
    private ModeloDeTabla tablaModelo;
    String[] nombreColumnas  = {"id","Cedula","Nombre y Apellido","Empresa","Departamento","Extension"};
    Object [] claseColumnas  = {0, "", "", "", "", 0};
    private JTable tabla;
    private TableColumn colId, colCedula, colNombre, colEmpresa, colDepartamento, colCExtension;
    private final Object[] datos = new Object[6];
    private JTextField campo;
    private final Usuarios usuarios;
    private ResultSet resultado;
    private Contenido contenido;
    
    /**
     * Construye la JTable.
     */
    public TablaUsuarios(Contenido contenido){
        this.contenido = contenido;
        this.setLayout(new BorderLayout());
        TitledBorder bordrer = BorderFactory.createTitledBorder("Listado");
                     bordrer.setTitleColor(Color.WHITE);
        this.setBorder(bordrer);
        this.setBackground(Colores.azulOscuro2);
        usuarios = new Usuarios();
        crearTabla();
    }
    public TablaUsuarios(){
        this(null);
    }
    final void crearTabla(){
        tablaModelo = new ModeloDeTabla(nombreColumnas, claseColumnas);
        campo = new JTextField();
        campo.setEditable(false);
        campo.setHorizontalAlignment(SwingConstants.CENTER);

        tabla = new ReizableJTable(tablaModelo);
        tabla.setFillsViewportHeight(true);
        tabla.addMouseListener(new EscuchaTablaItem(this));
        colId           = tabla.getColumnModel().getColumn(0);
        colCedula       = tabla.getColumnModel().getColumn(1);
        colNombre       = tabla.getColumnModel().getColumn(2);
        colEmpresa      = tabla.getColumnModel().getColumn(3);
        colDepartamento = tabla.getColumnModel().getColumn(4);
        colCExtension   = tabla.getColumnModel().getColumn(5);
        
        
        colId.setMinWidth(0);
        colId.setMaxWidth(0);
       /* colCedula.setMinWidth(100);
        colCedula.setMaxWidth(100);
        colNombre.setMinWidth(250);
        colNombre.setMaxWidth(250);
        colEmpresa.setMinWidth(200);
        colEmpresa.setMaxWidth(200);
        colDepartamento.setMinWidth(100);
        colDepartamento.setMaxWidth(100);*/
        colCExtension.setMinWidth(90);
        colCExtension.setMaxWidth(90);
        
        
        DefaultCellEditor ce = new DefaultCellEditor(campo);
        ce.setClickCountToStart(100);
        colCedula.setCellEditor(ce);
        colNombre.setCellEditor(ce);
        colEmpresa.setCellEditor(ce);
        colDepartamento.setCellEditor(ce);
        colCExtension.setCellEditor(ce);
        
        JScrollPane scrollPanel = new JScrollPane(tabla);
        this.add(scrollPanel);
    }
    
    /**
     * Carga la JTable con los datos que se pasaran como paramentros a traves de
     * un ResultSet.
     * @param data ResultSet contentivo de los datos a cargar en la tabla.
     * @return Verdadero si la carga fue exitosa, falso en caso contrario.
     */
    public boolean cargarTabla(ResultSet data){
        boolean todoBien = false;
        int filas = tabla.getRowCount();
        /**
         * El siguiente for borra los registros previamente cargados.
         */
        if(filas > 0) {
            for (int i = filas - 1; i >= 0; i--) {
                tablaModelo.removeRow(i);
            }
        }
        try{
           while(data.next()){
               datos[0] = Integer.parseInt(data.getString("usuario_id"));
               datos[1] = data.getString("usuario_cedula");
               datos[2] = data.getString("usuario_nombre") + " " + data.getString("usuario_apellido");
               datos[3] = data.getString("empresa");
               datos[4] = Usuarios.getUsuarioDepartamento(Integer.parseInt(data.getString("usuario_departamento")));
               datos[5] = data.getString("usuario_extension");
               tablaModelo.addRow(datos);
            }
           todoBien = true;
        }
        catch(SQLException e){
            String mensaje = errorSQL(e.getSQLState());
            JOptionPane.showMessageDialog(this,mensaje);
            System.exit(2000);
        }
       return todoBien; 
    }
    
    /**
     * Agrega un fila a la JTable con los datos pasados por parametros.
     * @param nombreLaboratorio Nombre del usuarios a agregar.
     * @return Verdadero si la inclusion fue exitosa, falso en caso contrario.
     */
    public boolean agregarFila(String usuario, String clave, 
                                  String cedula, String email,
                                  String nombre, String apellido, 
                                  int empresa, String sede, 
                                  int departamento, int extension){
        boolean todoBien = false;
        int usuarioId = usuarios.insertar(usuario,clave,cedula,email,nombre,apellido,empresa,sede,departamento,extension);
        if(usuarioId != 0){
            resultado = usuarios.sqlrRegistroId(usuarioId);
            try {
                if (resultado.next())
                    datos[1] = resultado.getString(1);
            }
            catch (SQLException ex) {
                ex.printStackTrace();
            }
            todoBien = true;
            tablaModelo.addRow(datos);
        }
        return todoBien;
    }
    
    /**
     * Modifica una fila a la JTable con los datos pasados por parametros.
     * @param nombreLaboratorio Nombre del usuarios a agregar.
     * @return Verdadero si la modificacion fue exitosa,falso en caso contrario.
     */
    public boolean modificarFila(String usuario, String clave, 
                                   String cedula, String email,
                                   String nombre, String apellido, 
                                   int empresa, String empresaNombre, String sede, 
                                   int departamento, String departamentoNombre, int extension){
        boolean todoBien = false;
        int fila = tabla.getSelectedRow();
        if (fila >= 0){
            if (usuarios.modificar(obtenerId(), usuario,clave,cedula,email,nombre,apellido,empresa,sede,departamento,extension)){
                tablaModelo.setValueAt(cedula, fila, 1);
                tablaModelo.setValueAt(nombre + " " + apellido, fila, 2);
                tablaModelo.setValueAt(empresaNombre, fila, 3);
                tablaModelo.setValueAt(departamentoNombre, fila, 4);
                tablaModelo.setValueAt(extension, fila, 5);
                todoBien = true;
            }
        }
        return todoBien;
    }
    
    /**
     * Elimina la fila que este seleccionada en la JTable.
     * @return Verdadero si la eliminación fue exitosa, falso en caso contrario.
     */
    public boolean eliminarFila(){
        int fila = tabla.getSelectedRow();
        boolean todoBien = false;
        if (fila >= 0){
            int respuesta = JOptionPane.showConfirmDialog(this,
                                               "¿Seguro quiere eliminar a: "+
                                               tablaModelo.getValueAt(fila, 1) + " " + tablaModelo.getValueAt(fila, 2));
            if (respuesta == JOptionPane.OK_OPTION){
                if (usuarios.eliminar((int)tabla.getValueAt(fila, 0))){
                    tablaModelo.removeRow(fila);
                    todoBien = true;
                }
            }   
        }
        return todoBien;
    }
    public boolean verificarSeleccionado() {
        return (tabla.getSelectedRow()>-1);
    }
    public int obtenerId(){
        int fila = tabla.getSelectedRow();
        if (fila >= 0)
            return (int)tabla.getValueAt(fila, 0);
        else
            return 0;
    }
    public String obtenerContenido(int columna) {
        int fila = tabla.getSelectedRow();
        return (String) tabla.getValueAt(fila, columna);
    }
    @Override
    public void editarAccion() {
        int fila = tabla.getSelectedRow();
        if(fila != -1) {
            new EditarUsuarios(obtenerId(), this, contenido);
        }
    }

    @Override
    public void eliminarAccion() {
        
    }
}
