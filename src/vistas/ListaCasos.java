/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.componentes.Botones;
import vistas.componentes.CampoTexto;
import Componentes.ComboTabla;
import com.itextpdf.text.DocumentException;
import controladores.AccionBoton;
import controladores.EscuhaBuscar;
import controladores.EscuhaEditar;
import controladores.EscuhaExportar;
import controladores.EscuhaImprimir;
import controladores.EscuhaLimpiar;
import controladores.EscuhaNuevo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import modelos.Casos;
import modelos.Sesion;
import vistas.componentes.CampoFecha;
import vistas.componentes.Colores;
import vistas.reportes.ReporteExcelCasosLista;
import vistas.reportes.ReportePdfCasosLista;

/**
 *
 * @author dvillanueva
 */
public class ListaCasos extends JPanel implements AccionBoton {
    
    JPanel panelFiltro, panelFiltroTextos, panelFiltroBotonera, panelTabla;
    CampoTexto solicitanteTexto, solicitanteCedulaTexto;
    CampoFecha fechaInicioTexto, fechaFinTexto;
    ComboTabla estatusCasos, tiposCasos;
    String[] arrayBotones = {"Buscar","Limpiar","Nuevo","Editar","Imprimir Pdf","Exportar Excel"};
    Color[] arrayBotonesColores = {Colores.azul,Colores.amarillo2,Colores.azul2,Colores.verde2,Colores.rojo2,Colores.verde};
    ImageIcon[] arrayBotonesIconos = {new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-28-search.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-551-erase.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/interface-2.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/glyphicons-151-edit.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/interface-1.png")),
                                      new ImageIcon(getClass().getResource("/vistas/imagenes/iconos/blancos/interface.png"))};
    Botones botonesFiltro;
    private final TablaCasos tablaCasos;
    private final Casos casos;
    private Contenido contenido;

    public ListaCasos(Contenido contenido){
        this();
        this.contenido = contenido;
    }
    public ListaCasos() {
        this.setBackground(Colores.blanco);
        this.setLayout(new BorderLayout());
        
        panelFiltro = new JPanel();
        panelFiltro.setPreferredSize(new Dimension(400,150));
        panelFiltro.setBackground(Colores.blanco);
        panelFiltro.setLayout(new GridLayout(2,1));
        panelFiltroTextos = new JPanel();
        panelFiltroTextos.setSize(50,100);
        panelFiltroTextos.setLayout(new FlowLayout());
        panelFiltroTextos.setBackground(Colores.blanco);
        solicitanteTexto = new CampoTexto("Solicitante",12);
        panelFiltroTextos.add(solicitanteTexto);
        solicitanteCedulaTexto = new CampoTexto("Solicitante Cedula",12);
        panelFiltroTextos.add(solicitanteCedulaTexto);
        fechaInicioTexto = new CampoFecha("Fecha Inicio");
        panelFiltroTextos.add(fechaInicioTexto);
        fechaFinTexto = new CampoFecha("Fecha Fin");
        panelFiltroTextos.add(fechaFinTexto);
        estatusCasos = new ComboTabla("Estatus");
        estatusCasos.cargarCombo("0",Casos.getCasosEstatus());
        panelFiltroTextos.add(estatusCasos);
        tiposCasos = new ComboTabla("Tipos");
        tiposCasos.cargarCombo("0",Casos.getCasosTipos());
        panelFiltroTextos.add(tiposCasos);
        panelFiltroBotonera = new JPanel();
        panelFiltroBotonera.setBackground(Colores.blanco);
        botonesFiltro = new Botones(arrayBotones, arrayBotonesColores, arrayBotonesIconos);
        botonesFiltro.setBackground(Colores.blanco);
        botonesFiltro.asignarOyente(0, new EscuhaBuscar(this));
        botonesFiltro.asignarOyente(1, new EscuhaLimpiar(this));
        botonesFiltro.asignarOyente(2, new EscuhaNuevo(this));
        botonesFiltro.asignarOyente(3, new EscuhaEditar(this));
        botonesFiltro.asignarOyente(4, new EscuhaImprimir(this));
        botonesFiltro.asignarOyente(5, new EscuhaExportar(this));
        
        panelFiltroBotonera.add(botonesFiltro);
        panelFiltro.add(panelFiltroTextos);
        panelFiltro.add(panelFiltroBotonera);
        
        panelTabla = new JPanel();
        panelTabla.setLayout(new BorderLayout());
        panelTabla.setBackground(Colores.azulOscuro2);
        
        tablaCasos = new TablaCasos();
        casos      = new Casos();
        tablaCasos.cargarTabla(casos.sqlrRegistros());
        
        panelTabla.add(tablaCasos);
        
        this.add(panelFiltro, BorderLayout.NORTH);
        this.add(panelTabla, BorderLayout.CENTER);
    }
    
    @Override
    public void buscarAccion() {
        tablaCasos.cargarTabla(casos.sqlrRegistros(Integer.parseInt(estatusCasos.obtenerId()),
                                                   Integer.parseInt(tiposCasos.obtenerId()),
                                                   solicitanteCedulaTexto.obtenerContenido(),
                                                   solicitanteTexto.obtenerContenido(),
                                                   fechaInicioTexto.obtenerContenido(),
                                                   fechaFinTexto.obtenerContenido()));
    }

    @Override
    public void limpiarAccion() {
        solicitanteTexto.cambiarContenido("");
        solicitanteCedulaTexto.cambiarContenido("");
        fechaInicioTexto.cambiarContenido("");
        fechaFinTexto.cambiarContenido("");
        estatusCasos.posicionarCombo(0);
        tiposCasos.posicionarCombo(0);
        buscarAccion();
    }

    @Override
    public void nuevoAccion() {
        new EditarCasos(0, tablaCasos, contenido);
    }

    @Override
    public void editarAccion() {
        new EditarCasos(tablaCasos.obtenerId(), tablaCasos, contenido);
    }

    @Override
    public void eliminarAccion() {
    }

    @Override
    public void imprimirAccion() {
        try {
            new ReportePdfCasosLista(Integer.parseInt(estatusCasos.obtenerId()),
                    Integer.parseInt(tiposCasos.obtenerId()),
                    solicitanteCedulaTexto.obtenerContenido(),
                    solicitanteTexto.obtenerContenido(),
                    fechaInicioTexto.obtenerContenido(),
                    fechaFinTexto.obtenerContenido());
        } catch (DocumentException ex) {
            
        }
    }

    @Override
    public void exportarAccion() {
        try {
            new ReporteExcelCasosLista(Integer.parseInt(estatusCasos.obtenerId()),
                    Integer.parseInt(tiposCasos.obtenerId()),
                    solicitanteCedulaTexto.obtenerContenido(),
                    solicitanteTexto.obtenerContenido(),
                    fechaInicioTexto.obtenerContenido(),
                    fechaFinTexto.obtenerContenido());
        } catch (IOException ex) {
        } catch (SQLException ex) {
        }
    }
}
