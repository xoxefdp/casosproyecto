/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.componentes.Botones;
import vistas.componentes.CampoTexto;
import Componentes.ComboTabla;
import controladores.AccionBoton;
import controladores.EscuhaBuscar;
import controladores.EscuhaLimpiar;
import controladores.EscuhaNuevo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import modelos.Empresas;
import modelos.Usuarios;
import vistas.componentes.Colores;
import vistas.componentes.buscar.TablaUsuariosBuscar;

/**
 *
 * @author usuario
 */
public class BuscarTercero implements AccionBoton{
    private int depart;
    private TablaUsuariosBuscar tablaUsuarios;
    private Usuarios usuarios;
    private Empresas empresa;
    private ResultSet datos;
    private final String condicion;
    String[] arrayBotones = {"Buscar","Limpiar","Agregar"};
    Color[] arrayBotonesColores = {Colores.azul,Colores.amarillo2,Colores.verde2};
    private Botones botonesFiltro;
    private JDialog modal;
    private final JDialog frame;
    private String title;
    private final JPanel panel;
    private final JPanel panelFiltroTextos;
    private final CampoTexto usuarioCedulaTexto;
    private final CampoTexto usuarioNombreTexto;
    private final ComboTabla usuarioEmpresa;
    private final CampoTexto campoAsignar;
    private final EditarCasos casosFrame;
    public BuscarTercero(JDialog frame) {
        this(frame, 0);
    }
    public BuscarTercero(JDialog frame, int depart) {
        this(frame,depart,null);
    }
    public BuscarTercero(JDialog frame, int depart, CampoTexto campoAsignar) {
        this(frame,depart,"", campoAsignar, null);
    }
    public BuscarTercero(JDialog frame, int depart, String condicion, CampoTexto campoAsignar, EditarCasos casosFrame) {
        this.frame = frame;
        this.depart = depart;
        this.condicion = condicion;
        this.campoAsignar = campoAsignar;
        this.casosFrame = casosFrame;
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        modal = new JDialog(frame, title, true);
        modal.setIconImage(new ImageIcon(getClass().getResource("/vistas/imagenes/glyphicons/glyphicons-28-search.png")).getImage());

        panelFiltroTextos = new JPanel();
        panelFiltroTextos.setBackground(Colores.blanco);
        panelFiltroTextos.setLayout(new FlowLayout());
        usuarioNombreTexto = new CampoTexto("Nombre y Apellido",12);
        panelFiltroTextos.add(usuarioNombreTexto);
        usuarioCedulaTexto = new CampoTexto("Cedula",12);
        panelFiltroTextos.add(usuarioCedulaTexto);
        usuarioEmpresa = new ComboTabla("Empresa");
        empresa = new Empresas();
        usuarioEmpresa.cargarCombo("0",empresa.sqlrRegistros());
        panelFiltroTextos.add(usuarioEmpresa);
        panel.add(panelFiltroTextos,BorderLayout.NORTH);
        
        botonesFiltro = new Botones(arrayBotones, arrayBotonesColores);
        botonesFiltro.setBackground(Colores.blanco);
        botonesFiltro.asignarOyente(0, new EscuhaBuscar(this));
        botonesFiltro.asignarOyente(1, new EscuhaLimpiar(this));
        botonesFiltro.asignarOyente(2, new EscuhaNuevo(this));
        panel.add(botonesFiltro,BorderLayout.CENTER);
        tablaUsuarios = new TablaUsuariosBuscar(modal,campoAsignar,casosFrame);
        usuarios      = new Usuarios();
        if(depart != 0) {
            if(condicion.isEmpty()) {
                datos = usuarios.sqlrRegistroDepartamento(depart);
            } else {
                datos = usuarios.sqlrRegistroDepartamento(depart,condicion);
            }
        } else {
            datos = usuarios.sqlrRegistros();
        }
        datos = usuarios.sqlrRegistros();
        tablaUsuarios.cargarTabla(datos);
        panel.add(tablaUsuarios,BorderLayout.SOUTH);
        modal.add(panel);
        modal.pack();
        modal.setLocationRelativeTo(frame);
        modal.setVisible(true);
    }

    @Override
    public void buscarAccion() {
        String idComboEmpre = ("0".equals(usuarioEmpresa.obtenerId())?"":usuarioEmpresa.obtenerId());
        tablaUsuarios.cargarTabla(usuarios.sqlrRegistros("",
                                                         usuarioCedulaTexto.obtenerContenido(),
                                                         usuarioNombreTexto.obtenerContenido(),
                                                         idComboEmpre, depart+""));
    }

    @Override
    public void limpiarAccion() {
        usuarioCedulaTexto.cambiarContenido("");
        usuarioNombreTexto.cambiarContenido("");
        usuarioEmpresa.posicionarCombo(0);
        buscarAccion();
    }
    @Override
    public void nuevoAccion() {
        if(tablaUsuarios.verificarSeleccionado()) {
            campoAsignar.cambiarContenido(tablaUsuarios.obtenerContenido(2));
            modal.dispose();
        } else {
            JOptionPane.showMessageDialog(modal, "Seleccione un Usuario.!!");
        }
    }

    @Override
    public void editarAccion() {
    
    }

    @Override
    public void eliminarAccion() {
    
    }

    @Override
    public void imprimirAccion() {
    
    }

    @Override
    public void exportarAccion() {
    
    }
    
}
