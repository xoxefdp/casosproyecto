/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import Componentes.ModeloDeTabla;
import static SQL.MensajesDeError.errorSQL;
import controladores.AccionTablaItem;
import controladores.EscuchaTablaItem;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import modelos.Casos;
import vistas.componentes.Colores;
import vistas.librerias.ReizableJTable;

/**
 *
 * @author dvillanueva
 */
public class TablaCasos extends JPanel implements AccionTablaItem {
    
    private ModeloDeTabla tablaModelo;
    String[] nombreColumnas  = {"# Caso","Estatus","Tipo","F.Creado","Solicitante","Cedula","Descripcion","Tecnico","F.Cerrado"};
    Object [] claseColumnas  = {0, "", "", "", "", "", "", "", ""};
    public JTable tabla;
    private TableColumn colId, colTipo, colNombre, colFecha, colCedula, colDescrip, colEstatus, colTec, colFechaCerrado;
    private final Object[] datos = new Object[9];
    private JTextField campo;
    private final Casos casos;
    private ResultSet resultado;
    private Contenido contenido;
    
    /**
     * Construye la JTable.
     */
    public TablaCasos(Contenido contenido){
        this();
        this.contenido = contenido;
    }
    public TablaCasos(){
        this.setLayout(new BorderLayout());
        TitledBorder bordrer = BorderFactory.createTitledBorder("Listado");
                     bordrer.setTitleColor(Color.WHITE);
        this.setBorder(bordrer);
        this.setBackground(Colores.azulOscuro2);
        casos = new Casos();
        crearTabla();
    }
    
    private final void crearTabla(){
        
        tablaModelo = new ModeloDeTabla(nombreColumnas, claseColumnas);
        campo = new JTextField();
        campo.setEditable(false);
        campo.setHorizontalAlignment(SwingConstants.CENTER);
        tabla = new ReizableJTable(tablaModelo) {

            private static final long serialVersionUID = 1L;
            private Border outside = new MatteBorder(1, 0, 1, 0, Color.red);
            private Border inside = new EmptyBorder(0, 1, 0, 1);
            private Border highlight = new CompoundBorder(outside, inside);

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component comp = super.prepareRenderer(renderer, row, column);
                JComponent jc = (JComponent) comp;
                comp.setForeground(Color.black);
                if(column == 1) {
                    int modelRow = convertRowIndexToModel(row);
                    String type = (String) getModel().getValueAt(modelRow, column);
                    Color  colorEstatus;
                    switch (type) {
                        case "Nuevo":
                            colorEstatus = (row % 2 == 0 ? Colores.azul2 : Colores.azul);
                            break;
                        case "Asignado":
                            colorEstatus = (row % 2 == 0 ? Colores.amarillo : Colores.amarillo2);
                            break;
                        case "Cerrado":
                            colorEstatus = (row % 2 == 0 ? Colores.verde : Colores.verde2);
                            break;
                        default: 
                            colorEstatus = (row % 2 == 0 ? Colores.azul2 : Colores.azul);
                            break;
                    }
                    comp.setForeground(Color.WHITE);
                    comp.setBackground(colorEstatus);
                    comp.setFont(new Font("Tahoma", Font.BOLD, 12));
                } else {
                    comp.setFont(tabla.getFont());
                }
                jc.setBorder(BorderFactory.createCompoundBorder(jc.getBorder(), BorderFactory.createEmptyBorder(0, 0, 0, 5)));
                return comp;
            }
        };
        tabla.setFillsViewportHeight(true);
        tabla.addMouseListener(new EscuchaTablaItem(this));
        colId           = tabla.getColumnModel().getColumn(0);
        colEstatus      = tabla.getColumnModel().getColumn(1);
        colTipo         = tabla.getColumnModel().getColumn(2);
        colFecha        = tabla.getColumnModel().getColumn(3);
        colNombre       = tabla.getColumnModel().getColumn(4);
        colCedula       = tabla.getColumnModel().getColumn(5);
        colDescrip      = tabla.getColumnModel().getColumn(6);
        colTec          = tabla.getColumnModel().getColumn(7);
        colFechaCerrado = tabla.getColumnModel().getColumn(8);
                
        colId.setMinWidth(50);
        colId.setMaxWidth(50);
        
        //colTipo.setMinWidth(150);
        //colTipo.setMaxWidth(150);
                
        colFecha.setMinWidth(150);
        colFecha.setMaxWidth(150);
                
        //colNombre.setMinWidth(150);
        //colNombre.setMaxWidth(150);
                
        colCedula.setMinWidth(120);
        colCedula.setMaxWidth(120);
                
        //colDescrip.setMinWidth(220);
        //colDescrip.setMaxWidth(220);
                
        colEstatus.setMinWidth(100);
        colEstatus.setMaxWidth(100);
                
        //colTec.setMinWidth(150);
        //colTec.setMaxWidth(150);  
        
        colFechaCerrado.setMinWidth(150);
        colFechaCerrado.setMaxWidth(150);  
        
        DefaultCellEditor ce = new DefaultCellEditor(campo);
        ce.setClickCountToStart(100);
        colId.setCellEditor(ce);
        colTipo.setCellEditor(ce);
        colFecha.setCellEditor(ce);
        colNombre.setCellEditor(ce);
        colCedula.setCellEditor(ce);
        colDescrip.setCellEditor(ce);
        colEstatus.setCellEditor(ce);
        colTec.setCellEditor(ce);
        colFechaCerrado.setCellEditor(ce);
        
        JScrollPane scrollPanel = new JScrollPane(tabla);
        add(scrollPanel);
    }
    
    /**
     * Carga la JTable con los datos que se pasaran como paramentros a traves de
     * un ResultSet.
     * @param data ResultSet contentivo de los datos a cargar en la tabla.
     * @return Verdadero si la carga fue exitosa, falso en caso contrario.
     */
    public boolean cargarTabla(ResultSet data){
        boolean todoBien = false;
        int filas = tabla.getRowCount();
        /**
         * El siguiente for borra los registros previamente cargados.
         */
        if(filas > 0) {
            for (int i = filas - 1; i >= 0; i--) {
                tablaModelo.removeRow(i);
            }
        }
        try{
            while(data.next()){
               datos[0] = Integer.parseInt(data.getString("casos_id"));
               datos[1] = Casos.getCasosEstatus(Integer.parseInt(data.getString("casos_estatus")));
               datos[2] = Casos.getCasosTipos(Integer.parseInt(data.getString("casos_tipo")));
               datos[3] = data.getString("fecha_creado");
               datos[4] = data.getString("solicitante");
               datos[5] = data.getString("solicitante_cedula");
               datos[6] = data.getString("casos_descripcion");
               datos[7] = data.getString("tecnico");
               datos[8] = data.getString("fecha_cerrado");
               tablaModelo.addRow(datos);
            }
           todoBien = true;
        }
        catch(SQLException e){
            String mensaje = errorSQL(e.getSQLState());
            JOptionPane.showMessageDialog(this,mensaje);
            System.exit(2000);
        }
       return todoBien; 
    }
    
    /**
     * Agrega un fila a la JTable con los datos pasados por parametros.
     * @param nombreLaboratorio Nombre del casos a agregar.
     * @return Verdadero si la inclusion fue exitosa, falso en caso contrario.
     */
    public boolean agregarFila(int casosTipo, int casosAsig,
                                  int casosEstatus, String casosDescrip,
                                  String casosObserv, int casosSoli, 
                                  int casosTecn){
        boolean todoBien = false;
        int casosId = casos.insertar(casosTipo,casosDescrip,casosObserv,casosSoli,casosTecn);
        if(casosId != 0){
            resultado = casos.sqlrRegistroId(casosId);
            try {
                if (resultado.next()) {
                    datos[0] = casosId;
                    datos[1] = Casos.getCasosEstatus(Integer.parseInt(resultado.getString("casos_estatus")));
                    datos[2] = Casos.getCasosTipos(Integer.parseInt(resultado.getString("casos_tipo")));
                    datos[3] = resultado.getString("fecha_creado");
                    datos[4] = resultado.getString("solicitante");
                    datos[5] = resultado.getString("solicitante_cedula");
                    datos[6] = resultado.getString("casos_descripcion");
                    datos[7] = resultado.getString("tecnico");
                    datos[8] = resultado.getString("fecha_cerrado");
                }
            }
            catch (SQLException ex) {
                ex.printStackTrace();
            }
            todoBien = true;
            tablaModelo.addRow(datos);
        }
        return todoBien;
    }
    
    /**
     * Modifica una fila a la JTable con los datos pasados por parametros.
     * @param nombreLaboratorio Nombre del casos a agregar.
     * @return Verdadero si la modificacion fue exitosa,falso en caso contrario.
     */
    public boolean modificarFila(int casosTipo, int casosAsig,
                                  int casosEstatus, String casosDescrip,
                                  String casosObserv, int casosSoli, 
                                  int casosTecn){
        boolean todoBien = false;
        int fila = tabla.getSelectedRow();
        if (fila >= 0){
            if (casos.modificar(obtenerId(),casosTipo,casosAsig,casosEstatus,casosDescrip,casosObserv,casosSoli,casosTecn)){
                resultado = casos.sqlrRegistroId(obtenerId());
                try {
                    if (resultado.next()) {
                        tablaModelo.setValueAt(Casos.getCasosEstatus(Integer.parseInt(resultado.getString("casos_estatus"))), fila, 1);
                        tablaModelo.setValueAt(Casos.getCasosTipos(Integer.parseInt(resultado.getString("casos_tipo"))), fila, 2);
                        tablaModelo.setValueAt(resultado.getString("fecha_creado"), fila, 3);
                        tablaModelo.setValueAt(resultado.getString("solicitante"), fila, 4);
                        tablaModelo.setValueAt(resultado.getString("solicitante_cedula"), fila, 5);
                        tablaModelo.setValueAt(resultado.getString("casos_descripcion"), fila, 6);
                        tablaModelo.setValueAt(resultado.getString("tecnico"), fila, 7);
                        tablaModelo.setValueAt(resultado.getString("fecha_cerrado"), fila, 8);
                    }
                }
                catch (SQLException ex) {
                    ex.printStackTrace();
                }
                todoBien = true;
            }
        }
        return todoBien;
    }
    
    /**
     * Elimina la fila que este seleccionada en la JTable.
     * @return Verdadero si la eliminación fue exitosa, falso en caso contrario.
     */
    public boolean eliminarFila(){
        int fila = tabla.getSelectedRow();
        boolean todoBien = false;
        if (fila >= 0){
            int respuesta = JOptionPane.showConfirmDialog(this,
                                               "¿Seguro quiere eliminar a: "+
                                               tablaModelo.getValueAt(fila, 1) + " " + tablaModelo.getValueAt(fila, 2));
            if (respuesta == JOptionPane.OK_OPTION){
                if (casos.eliminar((int)tabla.getValueAt(fila, 0))){
                    tablaModelo.removeRow(fila);
                    todoBien = true;
                }
            }   
        }
        return todoBien;
    }
    
    public int obtenerId(){
        int fila = tabla.getSelectedRow();
        if (fila >= 0)
            return (int)tabla.getValueAt(fila, 0);
        else
            return 0;
    }

    @Override
    public void editarAccion() {
        int fila = tabla.getSelectedRow();
        if(fila != -1) {
            new EditarCasos(obtenerId(), this, contenido);
        }
    }

    @Override
    public void eliminarAccion() {
        
    }
}
