/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.reportes;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import modelos.Usuarios;

/**
 *
 * @author Anthony
 */
public class ReportePdfUsuarioLista {
    private Usuarios usuarios;
    private ResultSet datos;
    private PdfPTable contenido;
    private Paragraph parrafo;
    private Document documento;
    private int pageNumber;
    private float[] anchos = {15,35,25,25,8};
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
    String strDate = sdfDate.format(new Date());
    private String rutaArchivo = System.getProperty("user.home")+"/reporteUsuariosLista" + strDate + ".pdf";
    private Image logo;
    private URL url = getClass().getResource("/vistas/imagenes/logo.png");
    private Chunk tituloText, tituloColumnas;
    private Font tituloFuente = new Font(FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.BLACK), tituloColumnaFuente = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    public ReportePdfUsuarioLista(String usuario, String cedula, String nombre, String empresa, String departamento) throws DocumentException {
        try {
            logo = Image.getInstance(url);
            logo.setAlignment(Image.ALIGN_LEFT);
            logo.scaleAbsoluteHeight(20);
            logo.scaleAbsoluteWidth(20);
            logo.scalePercent(30);
        } catch (BadElementException ex) {
            
        } catch (IOException ex) {
            
        }
        usuarios = new Usuarios();
        datos = usuarios.sqlrRegistros(usuario, cedula, nombre, empresa, departamento);
        pageNumber = 1;
        documento = new Document (PageSize.LETTER, 20, 20, 30, 30);
        contenido = new PdfPTable(anchos.length);
        contenido.setWidths(anchos);
        contenido.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);

        try {
            FileOutputStream archivoDeSalida = new FileOutputStream(rutaArchivo);
            PdfWriter.getInstance (documento, archivoDeSalida);
            documento.open();

            parrafo = new Paragraph();
            imprimeEncabezado();
            int lineas = 0;
            
            while (datos.next()){
                if (lineas > 10){
                    documento.add(contenido);
                    parrafo.setAlignment(Paragraph.ALIGN_RIGHT);
                    parrafo.add("Total personas en este reporte: " + (lineas+1) + "");
                    documento.add(parrafo);
                    parrafo.clear();     
                    documento.newPage();
                    lineas = 0;
                    pageNumber++;
                    imprimeEncabezado();
                }
                contenido.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
                PdfPCell cell = new PdfPCell(new Paragraph(datos.getString("usuario_cedula")));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                contenido.addCell(cell);
                contenido.addCell(datos.getString("usuario_nombre") + " " + datos.getString("usuario_apellido"));
                contenido.addCell(datos.getString("empresa"));
                contenido.addCell(Usuarios.getUsuarioDepartamento(Integer.parseInt(datos.getString("usuario_departamento"))));
                PdfPCell cell2 = new PdfPCell(new Paragraph(datos.getString("usuario_extension")));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                contenido.addCell(cell2);
                lineas++;
            }
            documento.add(contenido);
            documento.close ();
            archivoDeSalida.close();
            Desktop.getDesktop().open(new File(rutaArchivo));
        }
        catch (SQLException ioe){
            
        }
        catch (IOException ioe){
            
        }
        catch (DocumentException ioe){
            
        }        
        
    }
    private void imprimeEncabezado(){
        try{
            documento.add(logo);
            parrafo.setAlignment(Paragraph.ALIGN_RIGHT);
            parrafo.add("Fecha: " + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "/" + Calendar.getInstance().get(Calendar.MONTH) + "/" + Calendar.getInstance().get(Calendar.YEAR) + "");
            documento.add(parrafo);
            parrafo.clear();
            documento.add(parrafo);
            parrafo.clear();
            parrafo.add("Página: " + pageNumber + "");
            documento.add(parrafo);
            parrafo.clear();
            parrafo.add(" ");
            documento.add(parrafo);
            tituloText = new Chunk("Listado de Usuarios",tituloFuente);
            parrafo.add(tituloText);
            parrafo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(parrafo);
            parrafo.clear();
            parrafo.add(" ");
            documento.add(parrafo);
            parrafo.clear();
            documento.add(parrafo);
            contenido.flushContent();
            
            
            PdfPCell cell = new PdfPCell(new Paragraph("Cédula",tituloColumnaFuente)); 
            //cell.setColspan(2); // colspan 
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell);
            
            PdfPCell cell2 = new PdfPCell(new Paragraph("Apellidos y Nombres",tituloColumnaFuente)); 
            //cell2.setColspan(2); // colspan 
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell2.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell2);
            
            PdfPCell cell3 = new PdfPCell(new Paragraph("Empresa",tituloColumnaFuente)); 
            //cell3.setColspan(2); // colspan 
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell3.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell3);
            
            PdfPCell cell4 = new PdfPCell(new Paragraph("Departamento",tituloColumnaFuente)); 
            //cell4.setColspan(2); // colspan 
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell4.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell4);
            
            PdfPCell cell5 = new PdfPCell(new Paragraph("Ext.",tituloColumnaFuente));
            //cell5.setColspan(2); // colspan 
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell5.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell5);
        }
        catch(DocumentException de){
            
        }
        
    }
    
    
}
