/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.reportes;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import modelos.Casos;
import vistas.componentes.Colores;
import vistas.componentes.ColoresBasePdf;

/**
 *
 * @author Anthony
 */
public class ReportePdfCasosLista {
    private Casos casos;
    private ResultSet datos;
    private PdfPTable contenido;
    private Paragraph parrafo;
    private Document documento;
    private int pageNumber;
    private float[] anchos = {15,25,25,35,40,20,40,35,20};
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
    String strDate = sdfDate.format(new Date());
    private String rutaArchivo = System.getProperty("user.home")+"/reporteCasosLista" + strDate + ".pdf";
    private Image logo;
    private URL url = getClass().getResource("/vistas/imagenes/logo.png");
    private Chunk tituloText, tituloColumnas;
    private Font tituloFuente = new Font(FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.BLACK), tituloColumnaFuente = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    public ReportePdfCasosLista(int estatus, int tipo, String cedula, String nombre, String fechaInicio, String fechaFin) throws DocumentException {
        try {
            logo = Image.getInstance(url);
            logo.setAlignment(Image.ALIGN_LEFT);
            logo.scaleAbsoluteHeight(20);
            logo.scaleAbsoluteWidth(20);
            logo.scalePercent(30);
        } catch (BadElementException ex) {
            
        } catch (IOException ex) {
            
        }
        casos = new Casos();
        datos = casos.sqlrRegistros(estatus, tipo, cedula, nombre, fechaInicio, fechaFin);
        pageNumber = 1;
        documento = new Document (PageSize.A4.rotate(), 20, 20, 30, 30);
        documento.left(100f);
        documento.top(150f);        
        contenido = new PdfPTable(anchos.length);
        contenido.setWidths(anchos);
        contenido.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);

        try {
            FileOutputStream archivoDeSalida = new FileOutputStream(rutaArchivo);
            PdfWriter.getInstance (documento, archivoDeSalida);
            documento.open();

            parrafo = new Paragraph();
            imprimeEncabezado();
            int lineas = 0;
            
            while (datos.next()){
                if (lineas > 10){
                    documento.add(contenido);
                    parrafo.setAlignment(Paragraph.ALIGN_RIGHT);
                    parrafo.add("Total personas en este reporte: " + (lineas+1) + "");
                    documento.add(parrafo);
                    parrafo.clear();     
                    documento.newPage();
                    lineas = 0;
                    pageNumber++;
                    imprimeEncabezado();
                }
                contenido.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
                PdfPCell cell = new PdfPCell(new Paragraph(datos.getString("casos_id")));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                contenido.addCell(cell);
                PdfPCell cell2 = new PdfPCell(new Paragraph(Casos.getCasosEstatus(Integer.parseInt(datos.getString("casos_estatus"))), new Font(FontFamily.HELVETICA, 12, 0, BaseColor.WHITE)));
                String type = Casos.getCasosEstatus(Integer.parseInt(datos.getString("casos_estatus")));
                BaseColor  colorEstatus;
                switch (type) {
                    case "Nuevo":
                        colorEstatus = (lineas % 2 == 0 ? ColoresBasePdf.azul2 : ColoresBasePdf.azul);
                        break;
                    case "Asignado":
                        colorEstatus = (lineas % 2 == 0 ? ColoresBasePdf.amarillo : ColoresBasePdf.amarillo2);
                        break;
                    case "Cerrado":
                        colorEstatus = (lineas % 2 == 0 ? ColoresBasePdf.verde : ColoresBasePdf.verde2);
                        break;
                    default: 
                        colorEstatus = (lineas % 2 == 0 ? ColoresBasePdf.azul2 : ColoresBasePdf.azul);
                        break;
                }
                cell2.setBackgroundColor((BaseColor) colorEstatus);
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                contenido.addCell(cell2);
                contenido.addCell(Casos.getCasosTipos(Integer.parseInt(datos.getString("casos_tipo"))));
                contenido.addCell(datos.getString("fecha_creado"));
                contenido.addCell(datos.getString("solicitante"));
                contenido.addCell(datos.getString("solicitante_cedula"));
                contenido.addCell(datos.getString("casos_descripcion"));
                contenido.addCell(datos.getString("tecnico"));
                contenido.addCell(datos.getString("fecha_cerrado"));
                lineas++;
            }
            documento.add(contenido);
            documento.close ();
            archivoDeSalida.close();
            Desktop.getDesktop().open(new File(rutaArchivo));
        }
        catch (SQLException ioe){
            
        }
        catch (IOException ioe){
            
        }
        catch (DocumentException ioe){
            
        }        
        
    }
    private void imprimeEncabezado(){
        try{
            documento.add(logo);
            parrafo.setAlignment(Paragraph.ALIGN_RIGHT);
            parrafo.add("Fecha: " + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "/" + Calendar.getInstance().get(Calendar.MONTH) + "/" + Calendar.getInstance().get(Calendar.YEAR) + "");
            documento.add(parrafo);
            parrafo.clear();
            documento.add(parrafo);
            parrafo.clear();
            parrafo.add("Página: " + pageNumber + "");
            documento.add(parrafo);
            parrafo.clear();
            parrafo.add(" ");
            documento.add(parrafo);
            tituloText = new Chunk("Listado de Casos",tituloFuente);
            parrafo.add(tituloText);
            parrafo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(parrafo);
            parrafo.clear();
            parrafo.add(" ");
            documento.add(parrafo);
            parrafo.clear();
            documento.add(parrafo);
            contenido.flushContent();
            
            PdfPCell cell = new PdfPCell(new Paragraph("# Caso",tituloColumnaFuente)); 
            //cell.setColspan(2); // colspan 
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell);
            
            PdfPCell cell2 = new PdfPCell(new Paragraph("Estatus",tituloColumnaFuente)); 
            //cell2.setColspan(2); // colspan 
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell2.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell2);
            
            PdfPCell cell3 = new PdfPCell(new Paragraph("Tipo",tituloColumnaFuente)); 
            //cell3.setColspan(2); // colspan 
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell3.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell3);
            
            PdfPCell cell4 = new PdfPCell(new Paragraph("F.Creado",tituloColumnaFuente)); 
            //cell4.setColspan(2); // colspan 
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell4.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell4);
            
            PdfPCell cell5 = new PdfPCell(new Paragraph("Solicitante",tituloColumnaFuente));
            //cell5.setColspan(2); // colspan 
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell5.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell5);
            
            PdfPCell cell6 = new PdfPCell(new Paragraph("Cedula",tituloColumnaFuente));
            //cell5.setColspan(2); // colspan 
            cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell5.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell6);
            
            PdfPCell cell7 = new PdfPCell(new Paragraph("Descripcion",tituloColumnaFuente));
            //cell5.setColspan(2); // colspan 
            cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell5.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell7);
            
            PdfPCell cell8 = new PdfPCell(new Paragraph("Tecnico",tituloColumnaFuente));
            //cell5.setColspan(2); // colspan 
            cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell5.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell8);
            
            PdfPCell cell9 = new PdfPCell(new Paragraph("F.Cerrado",tituloColumnaFuente));
            //cell5.setColspan(2); // colspan 
            cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell5.setBackgroundColor(BaseColor.GRAY);
            contenido.addCell(cell9);
        }
        catch(DocumentException de){
            
        }
        
    }
    
    
}
