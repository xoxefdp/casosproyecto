/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import Componentes.ConexionBD;
import static SQL.MensajesDeError.errorSQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import vistas.Login;

/**
 *
 * @author dvillanueva
 */
public class Usuarios {
    private Statement  instruccionSql;
    private Connection conexion;
    private PreparedStatement pm;
    private ResultSet  resultados;
    private String sql, mensaje;
    private static String[] departamento  = {"Jefe Sistemas","Sistemas","Repuestos"};
    /**
    * Establece la conexión con la base de datos y la tabla Laboratorio.
    */
    public Usuarios(){
        conexion = ConexionBD.conectarBD();
        if (conexion == null)
            System.exit(1000);
        else{
            try{
                instruccionSql = conexion.createStatement();
            }
            catch(SQLException error){
                mensaje = errorSQL(error.getSQLState());
                JOptionPane.showMessageDialog(null, mensaje);
            }
        } 
    }
    public static String[] getUsuarioDepartamento() {
        return departamento;
    }
    public static String getUsuarioDepartamento(int pos) {
        return departamento[pos-1];
    }
    public final boolean sqlrRegistroValidarLogin(Login login, String usuario, String clave){
        String pass = "";
        int contResultado = 0;
        boolean sesion = false;
        try{
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario = ?\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, usuario);
            resultados = pm.executeQuery();
            while (resultados.next()){
                pass       = resultados.getString("usuario_contrasena");
                Sesion.setUsuarioId(Integer.parseInt(resultados.getString("usuario_id")));
                Sesion.setUsuarioNombre(resultados.getString("usuario_nombre") + " " + resultados.getString("usuario_apellido"));
                Sesion.setUsuarioDepartamento(Integer.parseInt(resultados.getString("usuario_departamento")));
                contResultado++;
            }
            if(contResultado > 0) {
                if(clave.equals(pass)) {
                    sesion = true;
                } else {
                    JOptionPane.showMessageDialog(login, "Error Contraseña");
                }
            } else {
                JOptionPane.showMessageDialog(login, "Error Usuario no Existe");
            }
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return sesion;
    }
    public final ResultSet sqlrRegistros(){
        try{
            sql = "Select u.*\n";
            sql+= "      ,e.empresa\n";
            sql+= "From usuario u\n";
            sql+= "Left Join empresa e on e.empresa_id = u.empresa_id\n";
            sql+= "Where u.usuario is not null\n";
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistros(String usuario, String cedula, String nombre, String empresa, String departamento){
        int posUsuario  = 1;
        int posCedula   = 2;
        int posNombre   = 4;
        int posEmpresa  = 5;
        int posDepart   = 6;
        try{
            sql = "Select u.*\n";
            sql+= "      ,e.empresa\n";
            sql+= "From usuario u\n";
            sql+= "Left Join empresa e on e.empresa_id = u.empresa_id\n";
            sql+= "Where u.usuario is not null\n";
            sql+= "and u.usuario_id != '1'\n";
            if(!usuario.isEmpty()) {
                sql+= "and u.usuario = ?\n";
            } else {
                posCedula   -= 1;
                posNombre   -= 1;
                posEmpresa  -= 1;
                posDepart   -= 1;
            }
            if(!cedula.isEmpty()) {
                sql+= "and u.usuario_cedula = ?\n";
            } else {
                posNombre   -= 1;
                posEmpresa  -= 1;
                posDepart   -= 1;
            }
            if(!nombre.isEmpty()) {
                sql+= "and (u.usuario_nombre Like ? or u.usuario_apellido Like ?)\n";
            } else {
                posEmpresa  -= 2;
                posDepart   -= 2;
            }
            if(!empresa.isEmpty()) {
                sql+= "and u.empresa_id = ?\n";
            } else {
                posDepart   -= 1;
            }
            if(!departamento.isEmpty()) {
                sql+= "and u.usuario_departamento = ?\n";
            }
            pm = conexion.prepareStatement(sql);
            if(!usuario.isEmpty()) {
                pm.setString(posUsuario, usuario);
            }
            if(!cedula.isEmpty()) {
                pm.setString(posCedula, cedula);
            }
            if(!nombre.isEmpty()) {
                pm.setString(posNombre-1, "%" + nombre + "%");
                pm.setString(posNombre, "%" + nombre + "%");
            }
            if(!empresa.isEmpty()) {
                pm.setString(posEmpresa, empresa);
            }
            if(!departamento.isEmpty()) {
                pm.setString(posDepart, departamento);
            }
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroId(int idUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_id = '" + idUsuario + "'\n";
            resultados = instruccionSql.executeQuery(sql); 
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroNombre(String nombreUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_nombre Like '%?%'\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, nombreUsuario);
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroApellido(String apellidoUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_apellido Like '%?%'\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, apellidoUsuario);
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroCedula(String cedulaUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_cedula = ?\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, cedulaUsuario);
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroEmail(String emailUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_email = ?\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, emailUsuario);
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroEmpresa(int empresaUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.empresa_id = '" + empresaUsuario + "'\n";
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroSede(String sedeUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_sede = ?\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, sedeUsuario);
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroDepartamento(int departamentoUsuario){
        try{  
            sql = "Select u.usuario_id\n";
            sql+= "       ,concat(ifnull(u.usuario_nombre,''),' ',ifnull(u.usuario_apellido,'')) as usuario_nombre\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_departamento = '" + departamentoUsuario + "'\n";
            resultados = instruccionSql.executeQuery(sql); 
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroDepartamento(int departamentoUsuario, String condicion){
        try{  
            sql = "Select u.usuario_id\n";
            sql+= "       ,concat(ifnull(u.usuario_nombre,''),' ',ifnull(u.usuario_apellido,'')) as usuario_nombre\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_departamento " + condicion + " '" + departamentoUsuario + "'\n";
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroExtension(String extensionUsuario){
        try{  
            sql = "Select u.*\n";
            sql+= "From usuario u\n";
            sql+= "Where u.usuario_extension = ?\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, extensionUsuario);
            resultados = pm.executeQuery();
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final int insertar(String usuario, String clave, 
                                  String cedula, String email,
                                  String nombre, String apellido, 
                                  int empresa, String sede, 
                                  int departamento, int extension){
        int inserccionOk = 0;
        try{  
            sql = "Insert into usuario\n";
            sql+= "(usuario, usuario_contrasena, usuario_cedula, usuario_email, usuario_nombre, usuario_apellido, empresa_id, usuario_sede, usuario_departamento, usuario_extension)\n";
            sql+= "Values\n";
            sql+= "(?, ?, ?, ?, ?, ?, '" + empresa + "', ?, '" + departamento + "', '" + extension + "');\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, usuario);
            pm.setString(2, clave);
            pm.setString(3, cedula);
            pm.setString(4, email);
            pm.setString(5, nombre);
            pm.setString(6, apellido);
            pm.setString(7, sede);
            pm.executeUpdate();
            sql = "Select LAST_INSERT_ID() as usuario_id;\n";
            resultados = instruccionSql.executeQuery(sql);
            while (resultados.next()){
                inserccionOk = Integer.parseInt(resultados.getString("usuario_id"));
            }
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return inserccionOk;
    }
    public final boolean modificar(int idUsuario, String usuario, String clave, 
                                   String cedula, String email,
                                   String nombre, String apellido, 
                                   int empresa, String sede, 
                                   int departamento, int extension){
        boolean updateOk = false;
        int posClave    = 2;
        int posCedula   = 3;
        int posEmail    = 4;
        int posNombre   = 5;
        int posApellido = 6;
        int posSede     = 7;
        try{  
            sql = "Update usuario\n";
            sql+= "Set usuario = ?\n";
            if(!clave.isEmpty()) {
                sql+= "   ,usuario_contrasena = ?\n";
            } else {
                posCedula   -= 1;
                posEmail    -= 1;
                posNombre   -= 1;
                posApellido -= 1;
                posSede     -= 1;
            }
            sql+= "   ,usuario_cedula = '"  + cedula + "'\n";
            sql+= "   ,usuario_email = '"  + email + "'\n";
            sql+= "   ,usuario_nombre = '"  + nombre + "'\n";
            sql+= "   ,usuario_apellido = '"  + apellido + "'\n";
            if(empresa != 0) {
                sql+= "   ,empresa_id = '"  + empresa + "'\n";
            }
            sql+= "   ,usuario_sede = '"  + sede + "'\n";
            sql+= "   ,usuario_departamento = '"  + departamento + "'\n";
            sql+= "   ,usuario_extension = '"  + extension + "'\n";
            sql+= "Where usuario_id = '" + idUsuario + "'\n";
            pm = conexion.prepareStatement(sql);
            pm.setString(1, usuario);
            pm.setString(posClave, clave);
            pm.setString(posCedula, cedula);
            pm.setString(posEmail, email);
            pm.setString(posNombre, nombre);
            pm.setString(posApellido, apellido);
            pm.setString(posSede, sede);
            pm.executeUpdate();
            updateOk = true;
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return updateOk;
    }
    public final boolean eliminar(int idUsuario){
        boolean eliminarOk = false;
        try{  
            sql = "Delete From usuario Where usuario_id = '" + idUsuario + "'\n";
            instruccionSql.executeUpdate(sql);
            eliminarOk = true;
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return eliminarOk;
    }
    
    /**
    * Cierra la conexion con la base de datos.
    */
    public void cerrarConexion(){
        try{  
            instruccionSql.close();
            conexion.close(); 
        }
	catch(SQLException error){
            JOptionPane.showMessageDialog(null,"Error en conexion. \n"+
                                          error);
            System.exit(200);
	}     
    }
}
