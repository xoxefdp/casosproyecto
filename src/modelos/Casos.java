/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import Componentes.ConexionBD;
import static SQL.MensajesDeError.errorSQL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import vistas.Login;

/**
 *
 * @author dvillanueva
 */
public class Casos {

    private Statement  instruccionSql;
    private Connection conexion;
    private ResultSet  resultados;
    private String sql, mensaje;
    private static String[] estatus  = {"Nuevo","Asignado","Cerrado"};
    private static String[] tipos  = {"InfoAuto","Solicitud de Requerimiento","Acceso a la Red",
                                      "Telefonia", "Movimiento de Equipos", "Instalacion de Software"};
    /**
    * Establece la conexión con la base de datos y la tabla Laboratorio.
    */
    public Casos(){
        conexion = ConexionBD.conectarBD();
        if (conexion == null)
            System.exit(1000);
        else{
            try{
                instruccionSql = conexion.createStatement();
            }
            catch(SQLException error){
                mensaje = errorSQL(error.getSQLState());
                JOptionPane.showMessageDialog(null, mensaje);
            }
        } 
    }
    public static String[] getCasosEstatus() {
        return estatus;
    }
    public static String getCasosEstatus(int pos) {
        if(pos == 0) {
            return "";
        }
        return estatus[pos-1];
    }
    public static String[] getCasosTipos() {
        return tipos;
    }
    public static String getCasosTipos(int pos) {
        if(pos == 0) {
            return "";
        }
        return tipos[pos-1];
    }
    public final ResultSet sqlrRegistros(){
        try{
            sql = "Select c.*\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_creado,'%d/%m/%Y %H:%i') as fecha_creado\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_asignado,'%d/%m/%Y %H:%i') as fecha_asignado\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_cerrado,'%d/%m/%Y %H:%i') as fecha_cerrado\n";
            sql+= "      ,concat(ifnull(s.usuario_nombre,''),' ',ifnull(s.usuario_apellido,'')) as solicitante\n";
            sql+= "      ,s.usuario_cedula as solicitante_cedula\n";
            sql+= "      ,concat(ifnull(t.usuario_nombre,''),' ',ifnull(t.usuario_apellido,'')) as tecnico\n";
            sql+= "From casos c\n";
            sql+= "Left Join usuario s on s.usuario_id = c.usuario_solicitante\n";
            sql+= "Left Join usuario t on t.usuario_id = c.usuario_tecnico\n";
            sql+= "Where c.casos_id is not null\n";
            if(Sesion.getUsuarioDepartamento() != 1) {
                if(Sesion.getUsuarioDepartamento() != 2) {
                   sql+= "and c.usuario_solicitante = '" + Sesion.getUsuarioId() + "'\n"; 
                } else {
                   sql+= "and c.usuario_tecnico = '" + Sesion.getUsuarioId() + "'\n"; 
                }
            }
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistros(int estatus, int tipo, String cedula, String nombre, String fechaInicio, String fechaFin){
        try{
            sql = "Select c.*\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_creado,'%d/%m/%Y %H:%i') as fecha_creado\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_asignado,'%d/%m/%Y %H:%i') as fecha_asignado\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_cerrado,'%d/%m/%Y %H:%i') as fecha_cerrado\n";
            sql+= "      ,concat(ifnull(s.usuario_nombre,''),' ',ifnull(s.usuario_apellido,'')) as solicitante\n";
            sql+= "      ,s.usuario_cedula as solicitante_cedula\n";
            sql+= "      ,concat(ifnull(t.usuario_nombre,''),' ',ifnull(t.usuario_apellido,'')) as tecnico\n";
            sql+= "From casos c\n";
            sql+= "Left Join usuario s on s.usuario_id = c.usuario_solicitante\n";
            sql+= "Left Join usuario t on t.usuario_id = c.usuario_tecnico\n";
            sql+= "Where c.casos_id is not null\n";
            if(estatus != 0) {
                sql+= "and c.casos_estatus = '" + estatus + "'\n";
            }
            if(tipo != 0) {
                sql+= "and c.casos_tipo = '" + tipo + "'\n";
            }
            if(!cedula.isEmpty()) {
                sql+= "and s.usuario_cedula = '" + cedula + "'\n";
            }
            if(!nombre.isEmpty()) {
                sql+= "and (s.usuario_nombre Like '%" + nombre + "%' or s.usuario_apellido Like '%" + nombre + "%')\n";
            }
            if(!fechaInicio.isEmpty() && fechaFin.isEmpty()) {
                sql+= "and DATE_FORMAT(c.casos_fecha_creado,'%d/%m/%Y') = '" + fechaInicio + "'\n";
            }
            if(!fechaFin.isEmpty() && fechaInicio.isEmpty()) {
                sql+= "and DATE_FORMAT(c.casos_fecha_creado,'%d/%m/%Y') = '" + fechaFin + "'\n";
            }
            if(!fechaInicio.isEmpty() && !fechaFin.isEmpty()) {
                sql += " and date(c.casos_fecha_creado) >= STR_TO_DATE('" + fechaInicio + "','%d/%m/%Y')\n"; 
                sql += " and date(c.casos_fecha_creado) <= STR_TO_DATE('"  + fechaFin + "','%d/%m/%Y')\n";
            }
            if(Sesion.getUsuarioDepartamento() != 1) {
                if(Sesion.getUsuarioDepartamento() != 2) {
                   sql+= "and c.usuario_solicitante = '" + Sesion.getUsuarioId() + "'\n"; 
                } else {
                   sql+= "and c.usuario_tecnico = '" + Sesion.getUsuarioId() + "'\n"; 
                }
            }
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistroId(int casosId){
        try{  
            sql = "Select c.*\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_creado,'%d/%m/%Y %H:%i') as fecha_creado\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_asignado,'%d/%m/%Y %H:%i') as fecha_asignado\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_cerrado,'%d/%m/%Y %H:%i') as fecha_cerrado\n";
            sql+= "      ,concat(ifnull(s.usuario_nombre,''),' ',ifnull(s.usuario_apellido,'')) as solicitante\n";
            sql+= "      ,s.usuario_cedula as solicitante_cedula\n";
            sql+= "      ,concat(ifnull(t.usuario_nombre,''),' ',ifnull(t.usuario_apellido,'')) as tecnico\n";
            sql+= "From casos c\n";
            sql+= "Left Join usuario s on s.usuario_id = c.usuario_solicitante\n";
            sql+= "Left Join usuario t on t.usuario_id = c.usuario_tecnico\n";
            sql+= "Where c.casos_id = '" + casosId + "'\n";
            if(Sesion.getUsuarioDepartamento() != 1) {
                if(Sesion.getUsuarioDepartamento() != 2) {
                   sql+= "and c.usuario_solicitante = '" + Sesion.getUsuarioId() + "'\n"; 
                } else {
                   sql+= "and c.usuario_tecnico = '" + Sesion.getUsuarioId() + "'\n"; 
                }
            }
            resultados = instruccionSql.executeQuery(sql); 
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final ResultSet sqlrRegistrosPie(){
        try{
            sql = "Select c.*\n";
            sql+= "      ,DATE_FORMAT(c.casos_fecha_creado,'%m/%Y') as fecha_filtro\n";
            sql+= "From casos c\n";
            sql+= "Where c.casos_id is not null\n";
            sql+= "and DATE_FORMAT(c.casos_fecha_creado,'%m-%Y') = DATE_FORMAT(now(),'%m-%Y')\n";
            if(Sesion.getUsuarioDepartamento() != 1) {
                if(Sesion.getUsuarioDepartamento() != 2) {
                   sql+= "and c.usuario_solicitante = '" + Sesion.getUsuarioId() + "'\n"; 
                } else {
                   sql+= "and c.usuario_tecnico = '" + Sesion.getUsuarioId() + "'\n"; 
                }
            }
            resultados = instruccionSql.executeQuery(sql);
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
            System.exit(200);
	}
        return resultados;
    }
    public final int insertar(int casosTipo, String casosDescrip,
                                  String casosObserv, int casosSoli, 
                                  int casosTecn){
        int inserccionOk = 0;
        try{  
            sql = "Insert into casos\n";
            sql+= "(casos_tipo, casos_estatus, casos_descripcion, casos_observacion, casos_autorizacion, usuario_solicitante, usuario_autorizador, usuario_tecnico, casos_fecha_creado)\n";
            sql+= "Values\n";
            sql+= "(" + casosTipo + ", 1, '" + casosDescrip + "', '" + casosObserv + "', null, " + casosSoli + ", 1, 1, now());\n";
            System.out.println(sql);
            instruccionSql.executeUpdate(sql);
            sql = "Select LAST_INSERT_ID() as caso_id;\n";
            resultados = instruccionSql.executeQuery(sql);
            while (resultados.next()){
                inserccionOk = Integer.parseInt(resultados.getString("caso_id"));
            }
        }
	catch(SQLException error){
            System.out.println("------<<<<<<<<<<");
            System.out.println(error.getSQLState());
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return inserccionOk;
    }
    public final boolean modificar(int idCaso, int casosTipo, int casosAsig,
                                  int casosEstatus, String casosDescrip,
                                  String casosObserv, int casosSoli, 
                                  int casosTecn){
        boolean updateOk = false;
        try{  
            sql = "Update casos\n";
            sql+= "Set casos_tipo = '"  + casosTipo + "'\n";
            sql+= "   ,casos_descripcion = '"  + casosDescrip + "'\n";
            sql+= "   ,casos_estatus = '"  + casosEstatus + "'\n";
            if(casosEstatus==2) {
                sql+= "   ,casos_fecha_asignado = now()\n";
            } else if(casosEstatus==3) {
                sql+= "   ,casos_fecha_cerrado = now()\n";
            }
            sql+= "   ,casos_observacion = '"  + casosObserv + "'\n";
            sql+= "   ,usuario_solicitante = '"  + casosSoli + "'\n";
            sql+= "   ,usuario_tecnico = '"  + casosTecn + "'\n";
            sql+= "Where casos_id = '" + idCaso + "'\n";
            instruccionSql.executeUpdate(sql);
            updateOk = true;
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return updateOk;
    }
    public final boolean modificarEstatus(int idCaso, int pos){
        boolean updateOk = false;
        try{  
            sql = "Update casos\n"; 
            sql+= "Set casos_status = '"  + Casos.getCasosEstatus(pos) + "'\n";
            if(pos == 2) {
                sql+= ", casos_fecha_cerrado = now()\n";
            }
            if(pos == 3) {
                sql+= ", casos_fecha_cerrado = now()\n";
            }
            sql+= "Where caso_id = '" + idCaso + "'\n";
            instruccionSql.executeUpdate(sql);
            updateOk = true;
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return updateOk;
    }
    public final boolean eliminar(int idUsuario){
        boolean eliminarOk = false;
        try{  
            sql = "Delete From caso Where caso_id = '" + idUsuario + "'\n";
            instruccionSql.executeUpdate(sql);
            eliminarOk = true;
        }
	catch(SQLException error){
            mensaje = errorSQL(error.getSQLState());
            JOptionPane.showMessageDialog(null,mensaje);
	}
        return eliminarOk;
    }
    
    /**
    * Cierra la conexion con la base de datos.
    */
    public void cerrarConexion(){
        try{  
            instruccionSql.close();
            conexion.close(); 
        }
	catch(SQLException error){
            JOptionPane.showMessageDialog(null,"Error en conexion. \n"+
                                          error);
            System.exit(200);
	}     
    }   
}
