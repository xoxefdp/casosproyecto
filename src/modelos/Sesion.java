/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author dvillanueva
 * Clase que cotiene los datos 
 * de usuario logueado en el sistema....
 */
public class Sesion {
    private static int usuarioId                    = 0;
    private static String usuarioNombre             = "";
    private static int usuarioDepartamento          = 0;

    public static int getUsuarioId() {
        return Sesion.usuarioId;
    }
    public static String getUsuarioNombre() {
        return Sesion.usuarioNombre;
    }
    public static int getUsuarioDepartamento() {
        return Sesion.usuarioDepartamento;
    }
    public static void setUsuarioId(int usuarioId) {
        Sesion.usuarioId = usuarioId;
    }
    public static void setUsuarioNombre(String usuarioNombre) {
        Sesion.usuarioNombre = usuarioNombre;
    }
    public static void setUsuarioDepartamento(int usuarioDepartamento) {
        Sesion.usuarioDepartamento = usuarioDepartamento;
    }
}
